const request = require('request')
const secrets = require('./secrets')

const helperGraph = {

  getMembersextensionAttribute12: function (nextUrl, token) {
    let options = {
      method: 'GET',
      url: nextUrl,
      headers:
        {
          'Authorization': token
        },
      json: true
    }

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve({setUsers: body.value, nextUrl: body['@odata.nextLink']})
        }
      })
    })

  },

  getToken: function (tenantId) {
    let options = {
      method: 'POST',
      url: `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
      headers:
        {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      form:
        {
          client_id: secrets.client_id,
          scope: secrets.scope,
          client_secret: secrets.client_secret,
          grant_type: secrets.grant_type
        }
    }

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(JSON.parse(body).access_token)
        }
      })
    })
  }

}

module.exports = helperGraph
