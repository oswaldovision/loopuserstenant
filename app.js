const {writeFileSync, appendFileSync} = require('fs')
const {getToken, getMembersextensionAttribute12} = require('./graphHelper')
const secrets = require('./secrets')
const async = require('asyncawait/async')
const await = require('asyncawait/await')
const targetFile = './users.csv'

let getNewToken = async(function (tenant) {
  let members = []
  let nextUrl = `https://graph.microsoft.com/beta/users?$filter=userType eq 'Member'`
  let token = await(getToken(tenant))

  while (nextUrl) {
    const data = await(getMembersextensionAttribute12(nextUrl, token))
    members.push(data.setUsers.map(user => {
      return {
        'userPrincipalName': user.userPrincipalName,
        'extensionAttribute12': user.onPremisesExtensionAttributes.extensionAttribute12
      }
    }))
    nextUrl = data.nextUrl
  }

  // Descomentar para escribir el resultado en un JSON
  // writeFileSync(targetFile, JSON.stringify(members))

  //
  members.forEach(set => {
    set.forEach(user => {
      appendFileSync(targetFile, `${user.userPrincipalName},${user.extensionAttribute12} \n`)
    })
  })

  return `total pages :  ${members.length}`
})

getNewToken(secrets.tenant).then(result => {
  console.log(result)
})